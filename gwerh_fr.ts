<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>&lt;a rel=&quot;license&quot; href=&quot;http://www.gnu.org/licenses/lgpl-3.0.en.html&quot;&gt;http://www.gnu.org/licenses/lgpl-3.0.en.html&lt;/a&gt;</source>
        <comment>Replace the &apos;.en.html&apos; by the appropriate (for instance &apos;.fr.html&apos;)</comment>
        <translation>&lt;a rel=&quot;license&quot; href=&quot;http://www.gnu.org/licenses/lgpl-3.0.fr.html&quot;&gt;http://www.gnu.org/licenses/lgpl-3.0.en.html&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="about.cpp" line="37"/>
        <source>About GpaWa Eye Rest Helper</source>
        <translation>À propos de GpaWa Eye Rest Helper</translation>
    </message>
    <message>
        <location filename="about.cpp" line="40"/>
        <source>/resources/about.png</source>
        <oldsource>/about.png</oldsource>
        <translation>/resources/a propos.png</translation>
    </message>
</context>
<context>
    <name>mainwin</name>
    <message>
        <location filename="mainwin.ui" line="38"/>
        <source>Start the 1h timer</source>
        <translation>Démarrer le compte
à rebours d&apos;1h</translation>
    </message>
    <message>
        <location filename="mainwin.ui" line="56"/>
        <source>Start the 5mn timer</source>
        <translation>Démarrer le compte
à rebours de 5mn</translation>
    </message>
    <message>
        <location filename="mainwin.ui" line="74"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="mainwin.ui" line="108"/>
        <source>Remaining time:</source>
        <translation>Temps restant :</translation>
    </message>
    <message>
        <location filename="mainwin.ui" line="145"/>
        <location filename="mainwin.cpp" line="108"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="mainwin.ui" line="263"/>
        <source>About GWERH</source>
        <translation>À propos de GWERH</translation>
    </message>
    <message>
        <location filename="mainwin.ui" line="281"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="98"/>
        <source>Start first timer</source>
        <translation>Lancer le premier compte à rebours</translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="103"/>
        <source>Start second timer</source>
        <translation>Lancer le second compte à rebours</translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="151"/>
        <location filename="mainwin.cpp" line="163"/>
        <source>Set this timer&apos;s time</source>
        <translation>Régler le compte à rebours</translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="151"/>
        <location filename="mainwin.cpp" line="163"/>
        <source>Time in minutes:</source>
        <translation>Temps en minutes :</translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="219"/>
        <source>Start the %1%2 timer</source>
        <translation>Démarrer le compte
à rebours de %1%2</translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="219"/>
        <source>%1h</source>
        <comment>hours</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwin.cpp" line="219"/>
        <source>%1mn</source>
        <comment>minutes</comment>
        <translation></translation>
    </message>
</context>
</TS>
