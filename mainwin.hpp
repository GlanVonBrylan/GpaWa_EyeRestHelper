#ifndef MAINWIN_H
#define MAINWIN_H

// //////////////////////////////////////////////////////////
//
// GWERH - GpaWa Eye Rest Helper
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Eye Rest Helper.
//
// GpaWa Eye Rest Helper is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Eye Rest Helper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Eye Rest Helper. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include <QWidget>
#include <QMessageBox>
#include <QLabel>
#include <QTimer>
#include <QSoundEffect>
#include <QApplication>
#include <QShortcut>


namespace Ui {
class mainwin;
}

enum Timer
{
	none, first, second, both
};

class mainwin : public QWidget
{
    Q_OBJECT

public:
    explicit mainwin(QWidget *parent = nullptr);
    ~mainwin();

public slots :
	void on_startTimer1_clicked();
	void on_startTimer2_clicked();
	void on_configTimer1Button_clicked();
	void on_configTimer2Button_clicked();
    void decreaseTimer();
	void on_stopTimer_clicked();

    void on_volume_sliderPressed();
    void on_volume_valueChanged(int value);
    void on_volume_sliderReleased();
    void volumeTooltip();
    void setVolume(int volume);

	int on_aboutGWERH_clicked();


private:
	void updateStartButtonDisplay(QAbstractButton* button, int time);
	void updateRemainingTimeDisplay();
	void saveSettings();

	static const QString VERSION_LABEL;

    Ui::mainwin *ui;

	Timer current;

	int timer1Time, timer2Time;

    int remainingMn;
    int remainingS;
    QTimer timer;
    QSoundEffect flute, bell;
    QShortcut escToStopTimer;
};

#endif // MAINWIN_H
