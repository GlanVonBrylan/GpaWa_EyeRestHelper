QT += widgets multimedia

CONFIG += C++14

SOURCES += \
    about.cpp \
    main.cpp \
    mainwin.cpp

HEADERS += \
    about.hpp \
    mainwin.hpp

FORMS += \
    mainwin.ui


TRANSLATIONS = gwerh_fr.ts


VERSION = 2.4
QMAKE_TARGET_PRODUCT = "GpaWa Eye Rest Helper"
QMAKE_TARGET_DESCRIPTION = "GpaWa Eye Rest Helper"
QMAKE_TARGET_COPYRIGHT = "Copyright (c) 2014-2024 Jules Renton--Epinette"

DEFINES += VERSION=\\\"$$VERSION\\\"

RC_ICONS = Icon.ico

DISTFILES += \
    CHANGELOG \
    README.md
