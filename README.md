![GpaWa Eye Rest Helper logo](https://i62.servimg.com/u/f62/17/10/54/90/logo_g10.png)

Current version : 2.3

# Presentation
**GpaWa Eye Rest Helper** (GWERH) is a piece of software that intents to help you take care of your eyes when your work on your computer for a long period.
The idea is that you are going to work for a rather long time, then rest your eyes, should it be by closing them, looking away, in short by avoiding to look at the screen, for a short period.
In order to help you doing that, GWERH offers you a long timer (one hour by default) which you start when you commence work. When the time is up, a sweet flute sound will tell you it is time to give your eyes a break. You then start the short timer (five minutes by default) which, once over, will continuously emit bell sounds, signalling you that you may get back to work. To stop the bells, click on the "Stop" button or start a timer.
Switch between phases of work and rest like this, and your eyes will thank you !

### Download pre-built executables
Include resources. If you only want the resources, download the GNU/Linux 64-bits version, as it is the most lightweight.<br />
Windows : https://www.mediafire.com/file/etnj7byr3jf9hp2/GpaWa_EyeRestHelper_setup.exe (9.52 Mio)<br />
GNU/Linux 64-bit : https://www.mediafire.com/file/izbf11nt0q7uf0p/GpaWa_Eye_Rest_Helper_2.2_-_Linux_64-bit.tar.xz (758 kio)<br/>
GNU/Linux 32-bit : https://www.mediafire.com/file/n7i7cf0y79tdyv7/GpaWa_Eye_Rest_Helper_2.2_-_Linux_32-bit.tar.xz (756 kio)

**GNU/Linux users :** Here are the packets you will have to install:
- libqt5widgets5
- libqt5multimedia5
- libqt5gui5
- libqt5core5
- libstdc++6
- libgcc_s1
- libc6

# Licence
**GpaWa Eye Rest Helper** is provided under the GNU Lesser General Public Licence version 3 (LGPL v3). For more details, see the COPYING.txt and COPYING_LESSER.txt files, or this link : [http://www.gnu.org/licenses/lgpl-3.0.en.html](http://www.gnu.org/licenses/lgpl-3.0.en.html) ![LGPL logo](http://www.gnu.org/graphics/lgplv3-147x51.png)

# Compiling
**GpaWa Eye Rest Helper** uses the [Qt framework](https://www.qt.io/). The easier way to compile it is therefore to install Qt Creator and to import the project in it.<br />
If you absolutely don't want to install Qt Creator...err...you should probably turn to non-Qt projects. Though I suppose compiling the source code of the framework, then using it as a normal library, should work. [Here it is. (384 Mio)](http://www.mediafire.com/file/6jadcgayw3lyy9p/qt-everywhere-src-5.10.0.7z) Note that I have not tried to compile this code, but this is the official source code given by The Qt Company, so if it does not work, it's not my fault.
