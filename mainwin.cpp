// //////////////////////////////////////////////////////////
//
// GWERH - GpaWa Eye Rest Helper
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Eye Rest Helper.
//
// GpaWa Eye Rest Helper is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Eye Rest Helper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Eye Rest Helper. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "mainwin.hpp"
#include "about.hpp"
#include "ui_mainwin.h"

#include <QInputDialog>
#include <QFile>
#include <QTextStream>
#include <QToolTip>

#define APPDIR QCoreApplication::applicationDirPath()
#define SETTINGS_FILE APPDIR + "/timers"

const QString mainwin::VERSION_LABEL("GpaWa Eye Rest Helper "+QString(VERSION));


mainwin::mainwin(QWidget *parent) : QWidget(parent), ui(new Ui::mainwin), current(none),
									timer1Time(60), timer2Time(5),
                                    remainingMn(0), remainingS(0), timer(this),
                                    escToStopTimer(QKeySequence("ESC"), this, SLOT(on_stopTimer_clicked()))
{
	ui->setupUi(this);

	qApp->setWindowIcon(QIcon(APPDIR + "/resources/logo.png"));

	setWindowTitle(VERSION_LABEL);
    ui->versionLabel->setText("<span style='color: gray;'>" + VERSION_LABEL + "</span>");
	ui->colon->setPixmap(QPixmap(APPDIR + "/resources/deux-points.png"));
    QIcon paramIcon(APPDIR + "/resources/param.png");
    ui->configTimer1Button->setIcon(paramIcon);
    ui->configTimer2Button->setIcon(paramIcon);

	if(QFile::exists(SETTINGS_FILE))
	{
		QFile settingsFile(SETTINGS_FILE);
		if(settingsFile.open(QFile::ReadOnly))
		{
			QTextStream stream(&settingsFile);
            int volume;
            stream>>timer1Time>>timer2Time>>volume;
			updateStartButtonDisplay(ui->startTimer1, timer1Time);
			updateStartButtonDisplay(ui->startTimer2, timer2Time);
            if(volume)
            {
                setVolume(volume);
                ui->volume->setValue(volume);
            }
		}
	}

	timer.setSingleShot(false);
	timer.setInterval(1000);

    flute.setSource(QUrl::fromLocalFile(APPDIR + "/resources/Flute.wav"));
    bell.setSource(QUrl::fromLocalFile(APPDIR + "/resources/Bells1.wav"));
    bell.setLoopCount(QSoundEffect::Infinite);

	QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(decreaseTimer()));
	QObject::connect(ui->aboutQt, SIGNAL(clicked()), qApp, SLOT(aboutQt()));

	setFixedSize(size());

    updateRemainingTimeDisplay();
}

mainwin::~mainwin()
{
    delete ui;
}


void mainwin::on_startTimer1_clicked()
{
	bell.stop();
	flute.stop();
	remainingMn = timer1Time;
	remainingS = 0;
	timer.start();

	updateRemainingTimeDisplay();

	current = first;
}

void mainwin::on_startTimer2_clicked()
{
	bell.stop();
	flute.stop();
	remainingMn = timer2Time;
	remainingS = 0;
	timer.start();

	updateRemainingTimeDisplay();

	current = second;
}

void mainwin::on_configTimer1Button_clicked()
{
	bool ok;
	timer1Time = QInputDialog::getInt(this, tr("Set this timer's time"), tr("Time in minutes:"), timer1Time, 1, 999, 1, &ok);

	if(ok)
	{
		updateStartButtonDisplay(ui->startTimer1, timer1Time);
		saveSettings();
	}
}

void mainwin::on_configTimer2Button_clicked()
{
	bool ok;
	timer2Time = QInputDialog::getInt(this, tr("Set this timer's time"), tr("Time in minutes:"), timer2Time, 1, 999, 1, &ok);

	if(ok)
	{
		updateStartButtonDisplay(ui->startTimer2, timer2Time);
		saveSettings();
	}
}

void mainwin::decreaseTimer()
{
	remainingS--;

	if(remainingS == -1)
	{
		remainingS = 59;
		remainingMn--;
	}

	if(remainingMn == -1)
	{
		remainingS = 0;
		remainingMn = 0;
		timer.stop();

		if(current == first)
			flute.play();
		else
			bell.play();

	}

	updateRemainingTimeDisplay();
}

void mainwin::on_stopTimer_clicked()
{
	bell.stop();
	flute.stop();
	timer.stop();
	remainingMn = 0;
	remainingS = 0;
	current = none;

	updateRemainingTimeDisplay();
}


void mainwin::on_volume_sliderPressed()
{
    bell.play();
    volumeTooltip();
}
void mainwin::on_volume_valueChanged(int value)
{
    setVolume(value);
    volumeTooltip();
    if(!ui->volume->isSliderDown())
        saveSettings();
}
void mainwin::on_volume_sliderReleased()
{
    bell.stop();
    saveSettings();
}

void mainwin::volumeTooltip()
{
    auto pos = ui->volumeIcon->pos();
    pos.setX(pos.x() + ui->volumeIcon->width());
    QToolTip::showText(pos, ui->volume->toolTip());
}

void mainwin::setVolume(int volume)
{
    flute.setVolume(volume / 100.f);
    bell.setVolume(volume / 100.f);
    ui->volume->setToolTip(tr("Volume: %1%").arg(volume));
}



int mainwin::on_aboutGWERH_clicked()
{
	About temp;
	return temp.exec();
}

void mainwin::updateStartButtonDisplay(QAbstractButton *button, int time)
{
	int minutes = time % 60, hours = time / 60;
	button->setText(tr("Start the %1%2 timer").arg(hours ? tr("%1h", "hours").arg(hours) : "").arg(minutes ? tr("%1mn", "minutes").arg(minutes) : ""));
}

void mainwin::updateRemainingTimeDisplay()
{
    ui->remainingMinutes->display(QString("%1").arg(remainingMn, 2, 10, QChar('0')));
    ui->remainingSeconds->display(QString("%1").arg(remainingS, 2, 10, QChar('0')));

	if(remainingMn || remainingS)
		setWindowTitle(QString::number(remainingMn) + ":"
					   + (remainingS < 10 ? "0" + QString::number(remainingS) : QString::number(remainingS))
					   + " - " + VERSION_LABEL);
	else
		setWindowTitle(VERSION_LABEL);
}

void mainwin::saveSettings()
{
	QFile settingsFile(SETTINGS_FILE);
	if(settingsFile.open(QFile::WriteOnly))
	{
		QTextStream stream(&settingsFile);
        stream<<timer1Time<<' '<<timer2Time<<' '<<(int)(bell.volume() * 100);
	}
}
