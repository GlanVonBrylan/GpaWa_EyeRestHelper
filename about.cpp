// //////////////////////////////////////////////////////////
//
// GWERH - GpaWa Eye Rest Helper
// Copyright 2014-2024 Jules Renton--Epinette (julesr1230@gmail.com)
//
// This software is provided under the GNU Lesser General Public License (GNU LGPL v3)
// See COPYING.txt and COPYING.LESSER.txt
//
// This file is part of GpaWa Eye Rest Helper.
//
// GpaWa Eye Rest Helper is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GpaWa Eye Rest Helper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with GpaWa Eye Rest Helper. If not, see <http://www.gnu.org/licenses/>.
//
// //////////////////////////////////////////////////////////


#include "about.hpp"
#include <QApplication>

About::About(QWidget *parent) : QDialog(parent, Qt::Dialog),
								layout(new QGridLayout()),
								picture(new QLabel()),
								text(new QLabel(tr("<a rel=\"license\" href=\"http://www.gnu.org/licenses/lgpl-3.0.en.html\">http://www.gnu.org/licenses/lgpl-3.0.en.html</a>", "Replace the '.en.html' by the appropriate (for instance '.fr.html')"))),
								ok(new QPushButton("Ok"))
{
	setWindowTitle(tr("About GpaWa Eye Rest Helper"));
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	picture->setPixmap(QPixmap(qApp->applicationDirPath() + tr("/resources/about.png")));

	text->setFont(QFont("Arial", 12));
	text->setOpenExternalLinks(true);

    QObject::connect(ok, SIGNAL(clicked()), this, SLOT(close()));

	layout->addWidget(picture, 0, 0, 1, 4);
	layout->addWidget(text, 3, 0, 1, 4);
    layout->addWidget(ok, 5, 3, 1, 1);
    setLayout(layout);
	setFixedSize(layout->sizeHint());
}

About::~About()
{
	// dtor
}
